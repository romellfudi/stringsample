package com.freddyfudi.sample;

import static org.junit.Assert.*;
import com.freddyfudi.sample.module.Component;
import java.util.List;
import java.util.Map;

import org.junit.Test;

/**
 * Created by romelldominguez on 1/21/17.
 */
public class ComponentTest {
    @Test
    public void startService() throws Exception {
        Component<List<Map<Integer, String>>> component = new Component<>();
        component.startService(new Component.Callback<List<Map<Integer, String>>>() {
            @Override
            public void returnObjects(List<Map<Integer, String>> objects) {
                assertEquals("Milagros", String.valueOf(objects.get(0).get(123)));
            }
        });
    }

}