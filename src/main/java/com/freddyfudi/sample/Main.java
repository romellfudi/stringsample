package com.freddyfudi.sample;

import java.util.List;
import java.util.Map;

import com.freddyfudi.sample.module.Component;
import com.freddyfudi.sample.util.Util;


/**
 * Created by romelldominguez on 1/21/17.
 */
public class Main {

//    Logger logger = new Logger();

    public static void main(String arg[]){
        String data = "0123456789";
        data = (data.length()%2==0)?data:"0".concat(data);
        for(int i=0;i<data.length()/2;i++){
            String temp = data.substring(i*2,i*2+2);
            Util.show(String.valueOf((char)String.valueOf(temp).hashCode()));
        }
        Util.show("----------------------");
        data = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for(int i=0;i<data.length();i++){
            Util.show(String.valueOf(String.valueOf(data.charAt(i)).hashCode()));
        }
        Util.show(String.valueOf(data.hashCode()));

        Component<List<Map<Integer,String>>> component = new Component<>();
        component.startService(new Component.Callback<List<Map<Integer, String>>>() {
            @Override
            public void returnObjects(List<Map<Integer, String>> objects) {
                Util.show(String.valueOf(objects.get(0).get(123)));
            }
        });
    }


}
