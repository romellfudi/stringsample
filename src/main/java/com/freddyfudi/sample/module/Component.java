package com.freddyfudi.sample.module;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by romelldominguez on 1/21/17.
 */
public class Component<T> {

    public void startService(Callback<T> callback) {
//        ...

        List<Map<Integer, String>> arrayList;
        arrayList = new ArrayList<>();
        Map<Integer, String> map;
        map = new HashMap<>();
        map.put(123, "Milagros");
        arrayList.add(map);
        callback.returnObjects((T) arrayList);
        //        ...
    }

    public static abstract class Callback<T> implements ReturnInterface<T> {

    }
}
