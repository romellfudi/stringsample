package com.freddyfudi.sample.module;

/**
 * Created by romelldominguez on 1/21/17.
 */
public interface ReturnInterface<T> {
    void returnObjects(T objects);
}
